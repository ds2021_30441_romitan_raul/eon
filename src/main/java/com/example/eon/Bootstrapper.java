package com.example.eon;

import com.example.eon.auth.AuthService;
import com.example.eon.auth.dto.SignupRequest;
import com.example.eon.entities.Device;
import com.example.eon.entities.Sensor;
import com.example.eon.entities.Measurement;
import com.example.eon.entities.user.Client;
import com.example.eon.repositories.DeviceRepository;
import com.example.eon.repositories.MeasurementRepository;
import com.example.eon.repositories.SensorRepository;
import com.example.eon.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
@RequiredArgsConstructor
public class Bootstrapper implements ApplicationListener<ApplicationReadyEvent> {
    private final AuthService authService;
    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;
    private final SensorRepository sensorRepository;
    private final MeasurementRepository sensorMeasurementRepo;

    @Value("${app.bootstrap}")
    private Boolean bootstrap;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        if(bootstrap) {
            System.out.println("Bootstrapping boooi");
            emptyAll();
            createUsers();
            createDevices();
            createMeasurements();
            System.out.println("Bootstrapping done boiiiii");
        }
    }
    public void emptyAll(){
        sensorMeasurementRepo.deleteAll();
        sensorMeasurementRepo.flush();
        sensorRepository.deleteAll();
        sensorRepository.flush();
        deviceRepository.deleteAll();
        deviceRepository.flush();
        userRepository.deleteAll();
        userRepository.flush();
    }

    public void createUsers(){
        authService.register(SignupRequest.builder()
                .email("raul.romitan@gmail.com")
                .username("root")
                .password("root123.")
                .build(), true);
        for(int i=0;i<4;i++){
            authService.register(SignupRequest.builder()
                    .email("client"+Integer.toString(i)+"@client.com")
                    .username("client"+Integer.toString(i))
                    .password("client123.")
                    .build(), false);
        }

    }

    public void createDevices(){
        final int page = 0;
        final int pageSize = 12;
        final Pageable pageable = PageRequest.of(page,pageSize);
        for(Client c : userRepository.findAllClients(pageable)){
            int noOfDevices = (int)(Math.random()*(2-1+1)+1);
            c.setDateOfBirth(LocalDateTime.now());
            for(int i=0;i<noOfDevices;i++){
                Sensor s = Sensor.builder()
                                .description("desc of senzor"+Integer.toString(i))
                                .maxValue((float) (Math.random()*(35-20+1)+20))
                                .client(c)
                                .build();
                deviceRepository.save(Device.builder()
                                .client(c)
                                .description("desc of device "+Integer.toString(i))
                                .location("location"+Integer.toString(i))
                                .maxConsumption((float) (Math.random()*(200-100+1)+100))
                                .sensor(s)
                                .build());
            }
        }
    }

    public void createMeasurements(){
        LocalDateTime current = LocalDateTime.now();
        for(Sensor s : sensorRepository.findAll()){
            LocalDateTime start = current.minusDays((long) (Math.random()*(3-1+1)+1));
            LocalDateTime end = current.plusDays((long) (Math.random()*(2-1+1)+1));
            while(start.isBefore(end)){
                sensorMeasurementRepo.save(Measurement.builder()
                                .measurementTime(start)
                                .energyConsumption((Double) (Math.random()*(s.getMaxValue()-0+1)))
                                .sensor(s)
                                .build());
                start=start.plusHours(1);
            }
        }
    }
}
