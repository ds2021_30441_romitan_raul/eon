package com.example.eon;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableRabbit
public class EonApplication {
    @Value("${queue.sensor.name}")
    private String sensorQueue;
    public static void main(String[] args) {
        SpringApplication.run(EonApplication.class, args);
    }

    @Bean
    public Queue queue(){
        return new Queue(sensorQueue, false);
    }

}
