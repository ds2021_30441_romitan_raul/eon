package com.example.eon;

public class UrlMapping {
    public static final String API_PATH = "/api";

    public static final String USERS = API_PATH + "/users";
    public static final String DEVICES = API_PATH + "/devices";
    public static final String SENSORS = API_PATH + "/sensors";
    public static final String MEASUREMENTS = API_PATH + "/measurements";
    public static final String CLIENTS = "/clients";

    public static final String CLIENT = "/client";
    public static final String DEVICE = "/device";
    public static final String SENSOR = "/sensor";


    public static final String SEARCH = "/search";
    public static final String ENTITY = "/{id}";

    public static final String AUTH = API_PATH + "/auth";
    public static final String SIGN_IN = "/sign-in";
    public static final String SIGN_UP = "/sign-up";
}
