package com.example.eon.auth.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class JwtResponse {
    private String token;
    private UUID id;
    private String username;
    private String email;
    private String role;
}
