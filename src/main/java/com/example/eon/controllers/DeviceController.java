package com.example.eon.controllers;

import com.example.eon.dtos.device.DeviceDTO;
import com.example.eon.dtos.device.DeviceMinimalDTO;
import com.example.eon.dtos.sensor.SensorMinimalDTO;
import com.example.eon.dtos.user.ClientMinimalDTO;
import com.example.eon.services.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.example.eon.UrlMapping.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(DEVICES)
@RequiredArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;

    @GetMapping
    public Page<DeviceMinimalDTO> allDevices(@PageableDefault(size = 10, sort = "location", direction = Sort.Direction.ASC) final Pageable pageable){
        return deviceService.getAll(pageable);
    }

    @GetMapping(CLIENT+ENTITY)
    public Page<DeviceMinimalDTO> allDevicesByClientId(@PathVariable UUID id, @PageableDefault(size = 10, sort = "location", direction = Sort.Direction.ASC) final Pageable pageable){
        return deviceService.findDevicesByClientId(id,pageable);
    }

    @GetMapping(ENTITY)
    public DeviceDTO getDeviceById(@PathVariable Long id) {
        return deviceService.getDevice(id);
    }

    @PostMapping
    public DeviceDTO create(@RequestBody DeviceMinimalDTO device){
        return deviceService.create(device);
    }

    @PutMapping(ENTITY)
    public DeviceDTO edit(@PathVariable Long id, @RequestBody DeviceMinimalDTO device){
        return deviceService.edit(id, device);
    }

    @PutMapping(ENTITY+"/{sensorId}")
    public DeviceDTO editWithSensor(@PathVariable Long id, @RequestBody DeviceMinimalDTO device, @PathVariable Long sensorId){
        return deviceService.editWithSensor(id, device, sensorId);
    }

    @DeleteMapping(ENTITY)
    public void delete(@PathVariable Long id) {
        deviceService.delete(id);
    }

}
