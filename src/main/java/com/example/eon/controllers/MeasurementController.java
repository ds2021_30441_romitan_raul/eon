package com.example.eon.controllers;

import com.example.eon.dtos.measurement.MeasurementDTO;
import com.example.eon.dtos.sensor.SensorDTO;
import com.example.eon.services.MeasurementService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import static com.example.eon.UrlMapping.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(MEASUREMENTS)
@RequiredArgsConstructor
public class MeasurementController {
    private final MeasurementService measurementService;
    @GetMapping(SENSOR+ENTITY)
    public Page<MeasurementDTO> allMeasurementsBySensorId(@PathVariable Long id, @PageableDefault(size = 10, sort = "id", direction = Sort.Direction.ASC) final Pageable pageable){
        return measurementService.getAllBySensorId(id, pageable);
    }

}
