package com.example.eon.controllers;

import com.example.eon.dtos.device.DeviceDTO;
import com.example.eon.dtos.device.DeviceMinimalDTO;
import com.example.eon.dtos.sensor.SensorDTO;
import com.example.eon.dtos.sensor.SensorMinimalDTO;
import com.example.eon.dtos.user.UserCreationDTO;
import com.example.eon.dtos.user.UserDTO;
import com.example.eon.entities.Sensor;
import com.example.eon.services.DeviceService;
import com.example.eon.services.SensorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.example.eon.UrlMapping.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(SENSORS)
@RequiredArgsConstructor
public class SensorController {
    private final SensorService sensorService;

    @GetMapping
    public Page<SensorMinimalDTO> allSensors(@PageableDefault(size = 10, sort = "id", direction = Sort.Direction.ASC) final Pageable pageable){
        return sensorService.getAll(pageable);
    }

    @GetMapping(CLIENT+ENTITY)
    public List<SensorMinimalDTO> allSensorsByClientId(@PathVariable UUID id){
        return sensorService.getAllByClientId(id);
    }

    @GetMapping(CLIENT+ENTITY+"/withoutDevice")
    public List<SensorMinimalDTO> allSensorsWithoutDeviceByClientId(@PathVariable UUID id){
        return sensorService.getAllWithoutDeviceByClientId(id);
    }

    @GetMapping(DEVICE+ENTITY)
    public SensorMinimalDTO getSensorByDeviceId(@PathVariable Long id){
        return sensorService.findSensorByDeviceId(id);
    }

    @GetMapping("/withoutDevice")
    public List<SensorMinimalDTO> getAllSensorsWithoutDevice(){
        return sensorService.getAllWithoutDevice();
    }

    @GetMapping(ENTITY)
    public SensorMinimalDTO getSensorById(@PathVariable Long id) {
        return sensorService.getSensor(id);
    }


    @PostMapping()
    public SensorMinimalDTO create(@RequestBody SensorMinimalDTO sensor){
        return sensorService.create(sensor);
    }

    @PutMapping(ENTITY)
    public SensorMinimalDTO edit(@PathVariable Long id, @RequestBody SensorMinimalDTO sensor){
        return sensorService.edit(id, sensor);
    }

    @DeleteMapping(ENTITY)
    public void delete(@PathVariable Long id) {
        sensorService.delete(id);
    }

}
