package com.example.eon.controllers;

import com.example.eon.auth.AuthService;
import com.example.eon.dtos.user.ClientDTO;
import com.example.eon.dtos.user.ClientMinimalDTO;
import com.example.eon.dtos.user.UserCreationDTO;
import com.example.eon.dtos.user.UserDTO;
import com.example.eon.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

import static com.example.eon.UrlMapping.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(USERS)
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<UserDTO> allUsers() {
        return userService.allUsersForList();
    }

    @GetMapping(CLIENTS)
    public Page<ClientMinimalDTO> allClients(@PageableDefault(size = 10, sort = "username", direction = Sort.Direction.ASC) final Pageable pageable) {
        return userService.allClientsForList(pageable);
    }

    @GetMapping(CLIENTS+SEARCH+"/{username}")
    public Page<ClientMinimalDTO> allClientsByUsername(@PathVariable String username, @PageableDefault(size = 10, sort = "username", direction = Sort.Direction.ASC) final Pageable pageable) {
        return userService.allClientsForListByUsername(username, pageable);
    }

    @PostMapping(CLIENTS)
    public UserDTO create(@RequestBody UserCreationDTO user){
        return userService.create(user);
    }

    @PutMapping(CLIENTS+ENTITY)//full edit
    public ClientMinimalDTO editClient(@PathVariable UUID id, @RequestBody ClientMinimalDTO client){
        return userService.editClient(id, client);
    }

    @PatchMapping(CLIENTS+ENTITY)//partial edit
    public UserDTO changePassword(@PathVariable Long id, @RequestBody String newPassword){
        return userService.changePassword(id, newPassword);
    }

    @GetMapping(CLIENTS+ENTITY)
    public ClientMinimalDTO getClientById(@PathVariable UUID id){
        return userService.getClient(id);
    }

    @DeleteMapping(CLIENTS+ENTITY)
    public void deleteUser(@PathVariable UUID id)
    {
        userService.delete(id);
    }
}
