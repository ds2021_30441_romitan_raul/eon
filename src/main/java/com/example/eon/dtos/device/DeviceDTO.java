package com.example.eon.dtos.device;

import com.example.eon.dtos.sensor.SensorDTO;
import com.example.eon.dtos.sensor.SensorMinimalDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDTO {
    private Long id;
    private String description;
    private String location;
    private float maxConsumption;
    private float averageConsumption;
    private SensorMinimalDTO sensor;
}
