package com.example.eon.dtos.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceMinimalDTO {
    private Long id;
    private String description;
    private String location;
    private float maxConsumption;
    private float averageConsumption;
}
