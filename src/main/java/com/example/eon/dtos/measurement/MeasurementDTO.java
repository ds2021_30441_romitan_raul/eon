package com.example.eon.dtos.measurement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class MeasurementDTO {
    private Long id;
    private LocalDateTime measurementTime;
    private float energyConsumption;
}
