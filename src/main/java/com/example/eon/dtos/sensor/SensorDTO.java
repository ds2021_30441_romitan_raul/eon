package com.example.eon.dtos.sensor;

import com.example.eon.dtos.measurement.MeasurementDTO;
import com.example.eon.entities.Device;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class SensorDTO {
    private Long id;
    private String description;
    private float maxValue;
    private Set<MeasurementDTO> measurements;
}
