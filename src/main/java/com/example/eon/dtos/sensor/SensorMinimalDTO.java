package com.example.eon.dtos.sensor;

import com.example.eon.entities.Device;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class SensorMinimalDTO {
    private Long id;
    private String description;
    private float maxValue;
}
