package com.example.eon.dtos.user;

import com.example.eon.dtos.device.DeviceDTO;
import com.example.eon.dtos.device.DeviceMinimalDTO;
import com.example.eon.entities.Device;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO {
    private UUID id;
    private String username;
    private String email;
    private LocalDateTime dateOfBirth;
    private Set<DeviceDTO> devices;
}
