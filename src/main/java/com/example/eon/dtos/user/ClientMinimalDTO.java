package com.example.eon.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ClientMinimalDTO {
    private UUID id;
    private String username;
    private String email;
    private LocalDateTime dateOfBirth;
}
