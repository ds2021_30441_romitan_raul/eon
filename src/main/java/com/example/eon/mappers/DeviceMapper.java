package com.example.eon.mappers;

import com.example.eon.dtos.device.DeviceDTO;
import com.example.eon.dtos.device.DeviceMinimalDTO;
import com.example.eon.entities.Device;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface DeviceMapper {
    DeviceMinimalDTO toMinimalDto(Device device);
    DeviceDTO toDto(Device device);
    Device fromMinimalDto(DeviceMinimalDTO device);
    Device fromDto(DeviceDTO device);
}
