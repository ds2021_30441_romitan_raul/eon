package com.example.eon.mappers;

import com.example.eon.dtos.measurement.MeasurementDTO;
import com.example.eon.entities.Measurement;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MeasurementMapper {
    MeasurementDTO measurementToDto(Measurement measurement);
}
