package com.example.eon.mappers;

import com.example.eon.dtos.device.DeviceDTO;
import com.example.eon.dtos.device.DeviceMinimalDTO;
import com.example.eon.dtos.sensor.SensorDTO;
import com.example.eon.dtos.sensor.SensorMinimalDTO;
import com.example.eon.entities.Device;
import com.example.eon.entities.Sensor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SensorMapper {
    SensorMinimalDTO toMinimalDto(Sensor sensor);
    SensorDTO toDto(Sensor sensor);
    Sensor fromMinimalDto(SensorMinimalDTO sensorMinimalDTO);
}
