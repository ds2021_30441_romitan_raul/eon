package com.example.eon.mappers;

import com.example.eon.dtos.user.*;
import com.example.eon.entities.user.Client;
import com.example.eon.entities.user.User;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMinimalDTO toMinimalDto(User user);

    @Mappings(
            @Mapping(target = "role", ignore = true)
    )
    UserDTO toDto(User user);

    User fromDto(UserDTO user);

    User fromCreationDto(UserCreationDTO user);

    ClientMinimalDTO clientToMinimalDto(Client client);
    ClientDTO clientToDto(Client client);
}
