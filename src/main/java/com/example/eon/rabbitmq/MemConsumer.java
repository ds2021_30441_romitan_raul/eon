package com.example.eon.rabbitmq;

import com.example.eon.entities.Measurement;
import com.example.eon.services.MeasurementService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class MemConsumer {
    public static MeasurementService measurementService;

    @RabbitListener(queues = "${queue.sensor.name}")
    public void receive(@Payload String data){
        System.out.println(data);
    }
}
