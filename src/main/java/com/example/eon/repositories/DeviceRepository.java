package com.example.eon.repositories;

import com.example.eon.entities.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface DeviceRepository extends JpaRepository<Device, Long> {
    Device findDeviceByClientId(UUID clientId);
    Device findDeviceBySensorId(Long id);
    Device findDeviceById(Long id);
    Page<Device> findAll(Pageable pageable);
    Page<Device> findAllByClientId(UUID id, Pageable pageable);
    List<Device> findAllByClientId(UUID id);
}
