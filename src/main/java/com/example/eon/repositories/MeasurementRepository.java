package com.example.eon.repositories;

import com.example.eon.entities.Measurement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface MeasurementRepository extends JpaRepository<Measurement, Long> {
    Page<Measurement> getAllBySensorId(Long id, Pageable pageable);
    List<Measurement> getAllBySensorIdAndMeasurementTimeBetween(Long id, LocalDateTime start, LocalDateTime end);
}
