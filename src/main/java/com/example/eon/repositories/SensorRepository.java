package com.example.eon.repositories;

import com.example.eon.entities.Sensor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface SensorRepository extends JpaRepository<Sensor, Long> {
    Sensor findByDeviceId(Long id);
    List<Sensor> findAllByDeviceIsNull();
    List<Sensor> findAllByClientId(UUID id);
    List<Sensor> findAllByClientIdAndDeviceIsNull(UUID id);
}
