package com.example.eon.repositories;

import com.example.eon.entities.user.Admin;
import com.example.eon.entities.user.Client;
import com.example.eon.entities.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByUsername(String username);

    Optional<Client> findClientById(UUID id);

    @Query(value = "SELECT * FROM user where user_type = 'Client' ", nativeQuery = true)
    Page<Client> findAllClients(Pageable pageable);

    Page<Client> findAllClientsByUsernameContaining(String username, Pageable pageable);

    @Query(value = "SELECT * FROM user where user_type = 'Admin' ", nativeQuery = true)
    List<Admin> findAllAdmins();

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    @Override
    @Modifying
    @Query(value = "delete from user", nativeQuery = true)
    void deleteAll();
}
