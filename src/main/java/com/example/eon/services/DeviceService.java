package com.example.eon.services;

import com.example.eon.dtos.device.DeviceDTO;
import com.example.eon.dtos.device.DeviceMinimalDTO;
import com.example.eon.dtos.user.ClientMinimalDTO;
import com.example.eon.entities.Device;
import com.example.eon.entities.Measurement;
import com.example.eon.entities.Sensor;
import com.example.eon.mappers.DeviceMapper;
import com.example.eon.repositories.DeviceRepository;
import com.example.eon.repositories.MeasurementRepository;
import com.example.eon.repositories.SensorRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DeviceService {
    private final DeviceRepository deviceRepository;
    private final SensorRepository sensorRepository;
    private final MeasurementRepository measurementRepository;
    private final DeviceMapper deviceMapper;

    public Page<DeviceMinimalDTO> getAll(Pageable pageable){
        return deviceRepository.findAll(pageable).map(deviceMapper::toMinimalDto);
    }

    public Page<DeviceMinimalDTO> findDevicesByClientId(UUID clientId, Pageable pageable){
        return deviceRepository.findAllByClientId(clientId, pageable).map(deviceMapper::toMinimalDto);
    }

    public DeviceDTO getDevice(Long id) {
        Device device = deviceRepository.findDeviceById(id);
        if(device.getSensor() != null) {
            double average = measurementRepository.getAllBySensorIdAndMeasurementTimeBetween(id, LocalDateTime.MIN, LocalDateTime.now()).stream().map(Measurement::getEnergyConsumption).collect(Collectors.toList()).stream().mapToDouble(Double::doubleValue).average().getAsDouble();
            device.setAverageConsumption((float) average);
        }else{
            device.setAverageConsumption(0.0f);
        }
        return deviceMapper.toDto(deviceRepository.findById(id).get());
    }

    public DeviceDTO create(DeviceMinimalDTO sensor) {
        return deviceMapper.toDto(deviceRepository.save(deviceMapper.fromMinimalDto(sensor)));
    }

    public DeviceDTO edit(Long id, DeviceMinimalDTO device){
        Device actDevice = deviceRepository.findDeviceById(id);
        actDevice.setDescription(device.getDescription());
        actDevice.setLocation(device.getLocation());
        actDevice.setMaxConsumption(device.getMaxConsumption());
        return deviceMapper.toDto(
                deviceRepository.save(actDevice)
        );
    }

    public DeviceDTO editWithSensor(Long id, DeviceMinimalDTO device, Long sensorId){
        Device actDevice = deviceRepository.findDeviceById(id);
        actDevice.setDescription(device.getDescription());
        actDevice.setLocation(device.getLocation());
        actDevice.setMaxConsumption(device.getMaxConsumption());
        actDevice.setSensor(sensorRepository.findById(sensorId).get());
        return deviceMapper.toDto(
                deviceRepository.save(actDevice)
        );
    }

    public void delete(Long id){
        Device actDevice = deviceRepository.findDeviceById(id);
        actDevice.setSensor(null);
        Sensor corespondentSensor = sensorRepository.findByDeviceId(id);
        corespondentSensor.setDevice(null);
        deviceRepository.deleteById(id);
    }
}
