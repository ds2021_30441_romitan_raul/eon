package com.example.eon.services;

import com.example.eon.dtos.measurement.MeasurementDTO;
import com.example.eon.mappers.MeasurementMapper;
import com.example.eon.repositories.MeasurementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MeasurementService {
    private final MeasurementRepository measurementRepository;
    private final MeasurementMapper measurementMapper;

    public Page<MeasurementDTO> getAllBySensorId(Long id, Pageable pageable){
        return measurementRepository.getAllBySensorId(id,pageable).map(measurementMapper::measurementToDto);
    }
}
