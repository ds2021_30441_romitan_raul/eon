package com.example.eon.services;

import com.example.eon.dtos.device.DeviceDTO;
import com.example.eon.dtos.device.DeviceMinimalDTO;
import com.example.eon.dtos.sensor.SensorDTO;
import com.example.eon.dtos.sensor.SensorMinimalDTO;
import com.example.eon.entities.Device;
import com.example.eon.entities.Measurement;
import com.example.eon.entities.Sensor;
import com.example.eon.entities.user.User;
import com.example.eon.mappers.SensorMapper;
import com.example.eon.repositories.DeviceRepository;
import com.example.eon.repositories.MeasurementRepository;
import com.example.eon.repositories.SensorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SensorService {
    private final SensorRepository sensorRepository;
    private final DeviceRepository deviceRepository;
    private final MeasurementRepository measurementRepository;
    private final SensorMapper sensorMapper;

    private Sensor findById(Long id) {
        return sensorRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Sensor not found: " + id));
    }

    public Page<SensorMinimalDTO> getAll(Pageable pageable){
        return sensorRepository.findAll(pageable).map(sensorMapper::toMinimalDto);
    }

    public List<SensorMinimalDTO> getAllByClientId(UUID id){
        return sensorRepository.findAllByClientId(id).stream().map(sensorMapper::toMinimalDto).collect(Collectors.toList());
    }

    public List<SensorMinimalDTO> getAllWithoutDeviceByClientId(UUID id){
        return sensorRepository.findAllByClientIdAndDeviceIsNull(id).stream().map(sensorMapper::toMinimalDto).collect(Collectors.toList());
    }

    public SensorMinimalDTO findSensorByDeviceId(Long deviceId){
        return sensorMapper.toMinimalDto(sensorRepository.findByDeviceId(deviceId));
    }


    public SensorMinimalDTO getSensor(Long id) {
        return sensorMapper.toMinimalDto(sensorRepository.findById(id).get());
    }

    public SensorMinimalDTO create(SensorMinimalDTO sensor) {
        Sensor actSensor = sensorRepository.getById(sensor.getId());
        LocalDateTime start = LocalDateTime.now().minusDays((long) (Math.random()*(10-1+1)+1));
        LocalDateTime end = LocalDateTime.now().plusDays((long) (Math.random()*(3-1+1)+1));
        while(start.isBefore(end)){
            measurementRepository.save(Measurement.builder()
                    .measurementTime(start)
                    .energyConsumption((Double) (Math.random()*(sensor.getMaxValue()-0+1)))
                    .sensor(actSensor)
                    .build());
            start=start.plusHours(1);
        }
        return sensorMapper.toMinimalDto(sensorRepository.save(sensorMapper.fromMinimalDto(sensor)));
    }

    public SensorMinimalDTO edit(Long id, SensorMinimalDTO sensor){
        Sensor actSensor = findById(id);
        actSensor.setDescription(sensor.getDescription());
        actSensor.setMaxValue(sensor.getMaxValue());
        return sensorMapper.toMinimalDto(
                sensorRepository.save(actSensor)
        );
    }

    public void delete(Long id){
        Device corespondentDevice = deviceRepository.findDeviceBySensorId(id);
        corespondentDevice.setSensor(null);
        sensorRepository.deleteById(id);
    }

    public List<SensorMinimalDTO> getAllWithoutDevice(){
        return sensorRepository.findAllByDeviceIsNull().stream().map(sensorMapper::toMinimalDto).collect(Collectors.toList());
    }
}
