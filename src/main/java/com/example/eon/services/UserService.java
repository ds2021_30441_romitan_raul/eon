package com.example.eon.services;

import com.example.eon.auth.AuthService;
import com.example.eon.dtos.user.ClientDTO;
import com.example.eon.dtos.user.ClientMinimalDTO;
import com.example.eon.dtos.user.UserCreationDTO;
import com.example.eon.dtos.user.UserDTO;
import com.example.eon.entities.Device;
import com.example.eon.entities.user.Client;
import com.example.eon.entities.user.User;
import com.example.eon.mappers.UserMapper;
import com.example.eon.repositories.DeviceRepository;
import com.example.eon.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final UserMapper userMapper;

//    public List<UserMinimalDTO> allUsersMinimal() {
//        return userRepository.findAll()
//                .stream().map(userMapper::toMinimalDto)
//                .collect(toList());
//    }

    public List<UserDTO> allUsersForList() {
        return userRepository.findAll()
                .stream().map(userMapper::toDto)
                .collect(toList());
    }

    public Page<ClientMinimalDTO> allClientsForList(Pageable pageable){
        return userRepository.findAllClients(pageable).map(userMapper::clientToMinimalDto);
    }


    public UserDTO edit(UUID id, UserDTO user) {
        User actUser = findById(id);
        actUser.setEmail(user.getEmail());
        actUser.setUsername(user.getUsername());

        return userMapper.toDto(
                userRepository.save(actUser)
        );
    }

    private User findById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User not found: " + id));
    }

    public UserDTO changePassword(Long id, String newPassword) {
        return null;
    }

    public UserDTO get(UUID id) {
        return userMapper.toDto(userRepository.findById(id).get());
    }

    public void delete(UUID id) {
        userRepository.deleteById(id);
    }

    public UserDTO create(UserCreationDTO user) {
        if (userRepository.existsByUsername(user.getUsername()))
            throw new RuntimeException("User with username:" + user.getUsername()+" already exists");
        if (userRepository.existsByEmail(user.getEmail()))
            throw new RuntimeException("User with email:" + user.getUsername()+" already exists");

        Client actUser = Client.builder()
                        .username(user.getUsername())
                        .email(user.getEmail())
                        .password(encoder.encode(user.getPassword()))
                        .build();
        return userMapper.toDto(userRepository.save(actUser));
    }
    public ClientMinimalDTO editClient(UUID id, ClientMinimalDTO client) {
        Client actClient = userRepository.findClientById(id).get();
        actClient.setEmail(client.getEmail());
        actClient.setUsername(client.getUsername());
        actClient.setDateOfBirth(client.getDateOfBirth());

        return userMapper.clientToMinimalDto(
                userRepository.save(actClient)
        );
    }

    public Page<ClientMinimalDTO> allClientsForListByUsername(String username, Pageable pageable) {
        return userRepository.findAllClientsByUsernameContaining(username, pageable).map(userMapper::clientToMinimalDto);
    }

    public ClientMinimalDTO getClient(UUID id) {
        for(Device d : userRepository.findClientById(id).get().getDevices()){
            System.out.print(d.getId());
        }
        return userMapper.clientToMinimalDto(userRepository.findClientById(id).get());
    }
}
